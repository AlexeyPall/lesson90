
from AcoModels import AcoModel
from AcoModels import AcoModelSet
from ConstructGraph import Graph
from ConstructGraph import ConstructGraph
from ArkReader import ArkReader
from GaussMixCompCache import GaussMixCompCache

class Token:

    def __init__(self, state, dist, sentence, prev_states_queue=[]):

        self.state = state
        self.dist = dist
        self.sentence = sentence
        self.debug_states_queue = prev_states_queue + [state]

    def __str__(self):
        return "word: {}; is_final: {}; dist: {}; sent: {}".format(self.state.word,
                                                                   self.state.is_final,
                                                                   self.dist,
                                                                   self.sentence)

# class TokenClass:
#
#     Token = namedtuple('Token', ['state',
#                                  'dist',
#                                  'sentence'])


class TokenPassing:

    def __init__(self, graph_head, wd_add, aco_model_set):
        """ """

        ''' Init pointer to graph head (start node for the algorithm) '''
        self.head = graph_head

        ''' '''
        self.wd_add = wd_add

        ''' Create cache for Gause mixes '''
        self.gause_cache = GaussMixCompCache(aco_model_set)

        self.phoneme_id = [_ph for _ph in aco_model_set.name2model.keys()]

    def run(self, data_to_recognize, thr_common=float('inf'), max_tokens=float('inf')):
        """ """

        ''' Init set with tokens '''
        next_tokens = {id(self.head): Token(state=self.head,
                                            dist=0,
                                            sentence="")}

        ''' Calculate new tokens with new feature vector '''
        for _feature in data_to_recognize:
            # print('Step with ftr: ', _feature)
            # for _, _token in next_tokens.items():
            #     print(str(_token))
            next_tokens = self.passing_step(next_tokens, _feature, thr_common, max_tokens)

            # idx += 1
            # if idx % 10 == 0:
            #     print('idx: {} tokens: {}'.format(idx, len(next_tokens)))

        ''' Get final tokens '''
        final_tokens = []

        for _, _token in next_tokens.items():
            if _token.state.is_final:
                final_tokens.append(_token)
                # print(str(_token))

        ''' Print winner token '''
        win_token = None
        dist_token = float('inf')
        for _token in next_tokens.values(): #final_tokens:
            if _token.dist < dist_token:
                win_token = _token
                dist_token = _token.dist

        # print('Winner :\n\t', str(win_token))
        return win_token.sentence, final_tokens

    @staticmethod
    def save_recognized_transcripts(final_tokens, rec_file_name):
        """  """

        with open(rec_file_name, 'w') as f:
            for _token in final_tokens:
                f.write(_token.state.word + ' ' + _token.sentence + '\n')

    def passing_step(self, next_tokens, feature, thr_common, max_tokens):
        """ """

        ''' Set next_tokens as active tokens. Clear the next_tokens '''
        active_tokens = next_tokens
        next_tokens = dict()

        ''' Init min_distance to realize thm_common '''
        min_dist = float('inf')

        ''' Create cache to calculate distance between states ftr and a input ftr '''
        distance_cache = {}

        ''' Set frame in Gause cache '''
        self.gause_cache.setFrame(feature)

        ''' Each token spawns new tokens '''
        for _, _token in active_tokens.items():

            min_dist = min(min_dist, self.add_next_tokens(_token, _token.state.links_to_next_state,
                                                          next_tokens, min_dist, distance_cache, feature))

            # ''' Generate all possible new tokens '''
            # for _link_to_state in _token.state.links_to_next_state:
            #
            #     ''' '''
            #     new_dist = (_token.dist +
            #                 TokenPassing.calc_dist_to_state(distance_cache, _link_to_state.state, feature))
            #
            #     ''' If token in the same state has less dist then do nothing '''
            #     if id(_link_to_state.state) in next_tokens and \
            #             next_tokens[id(_link_to_state.state)].dist <= new_dist:
            #         continue
            #
            #     ''' Create new token '''
            #     new_token = Token(state=_link_to_state.state,
            #                       dist=new_dist,
            #                       sentence=_token.sentence + _link_to_state.state.model.name + " ",
            #                       prev_states_queue=_token.debug_states_queue)
            #
            #     ''' Add new token to the next tokens '''
            #     next_tokens[id(_link_to_state.state)] = new_token
            #
            #     ''' Save min distance for thm_common '''
            #     min_dist = min(min_dist, new_dist)

        ''' beam pruning '''
        # print('len before : ', len(next_tokens))
        for _state in list(next_tokens.keys()):
            if next_tokens[_state].dist > min_dist + thr_common:
                del next_tokens[_state]

        if len(next_tokens) > max_tokens:
            list_with_tokens = list(zip(next_tokens.values(), next_tokens.keys()))
            list_with_tokens = sorted(list_with_tokens, key=lambda x: x[0].dist)

            for i in range(max_tokens, len(list_with_tokens)):
                del next_tokens[list_with_tokens[i][1]]

        return next_tokens

    def add_next_tokens(self, _token, list_of_states, next_tokens, min_dist, distance_cache, feature):
        """ """

        ''' Generate all possible new tokens '''
        for _link_to_state in list_of_states:

            ''' If next state is Head '''
            if id(_link_to_state.state) == id(self.head):
                min_dist = min(min_dist, self.add_next_tokens(_token, self.head.links_to_next_state,
                                                              next_tokens, min_dist, distance_cache, feature))
                continue

            ''' '''
            new_dist = (_token.dist +
                        self.calc_dist_to_state(distance_cache, _link_to_state.state, feature))

            if _link_to_state.state.is_final \
               and id(_link_to_state.state) != id(_token.state):
               #and _link_to_state.state.model.name != 'SIL':
                new_dist += self.wd_add

            ''' If token in the same state has less dist then do nothing '''
            if id(_link_to_state.state) in next_tokens and \
                            next_tokens[id(_link_to_state.state)].dist <= new_dist:
                continue

            ''' Create new token '''
            new_token = Token(state=_link_to_state.state,
                              dist=new_dist,
                              sentence=_token.sentence, # + _link_to_state.state.word, #_link_to_state.state.model.name + " ",
                              prev_states_queue=_token.debug_states_queue)

            if id(_link_to_state.state) != id(_token.state):
                new_token.sentence += _link_to_state.state.word + ' '

            ''' Add new token to the next tokens '''
            next_tokens[id(_link_to_state.state)] = new_token

            ''' Save min distance for thm_common '''
            min_dist = min(min_dist, new_dist)

        return min_dist

    @staticmethod
    def beam_pruning(next_tokens, min_dist, thr_common):
        """ """

        for _state in list(next_tokens.keys()):
            if next_tokens[_state].dist > min_dist + thr_common:
                del next_tokens[_state]


    def calc_dist_to_state(self, distance_cache, state, feature):
        """ """

        return self.gause_cache.getDist(self.phoneme_id, state.model.name)

        # if id(state) not in distance_cache:
        #     distance_cache[id(state)] = self.gause_cache.getDist(
        #         self.phoneme_id, state.model.name) #state.model.dist(feature)
        #
        # return distance_cache[id(state)]


if __name__ == '__main__':

    aco_model = AcoModelSet.load_aco_model_set('data/VoxForge/ph_models_MIX_sk_10_20')
    head_node = ConstructGraph.costruct('data/DaNetSeq/DaNetSil.dic', aco_model)

    # print('\n' + '#' * 30 + '\n' + ' Debug '.center(30, '~') + '\n' + '#'*30)
    # ConstructGraph.print_graph(head_node)

    features_to_recognize = ArkReader()
    features_to_recognize.read_file('data/DaNetSeq/DaNetSeq.txtftr')

    engine = TokenPassing(head_node, 100)

    print('\n' + '-' * 30 + '\n' + ' File 1 '.center(30, '~') + '\n' + '-'*30)
    word, _ = engine.run(features_to_recognize.get_feature('net-net.wav'))
    print('Word : ', word)
    # print('\n' + '-' * 30 + '\n' + ' File 2 '.center(30, '~') + '\n' + '-'*30)
    # engine.run(features_to_recognize.get_feature('file2'))
    # print('\n' + '-' * 30 + '\n' + ' File 3 '.center(30, '~') + '\n' + '-'*30)
    # engine.run(features_to_recognize.get_feature('file3'))
