import os
import numpy as np
import pickle
import gmm
from scipy.cluster.vq import kmeans
from sklearn.mixture import GaussianMixture


coeff_vector = [1.0, 0.9, 0.4, 0.4, 0.4, 0.0, 0.0, 0.0, 0.1, 0.1, 0.05, 0.05, 0.01]

''' Default value for count clusters '''

NUM_GAUS_MIXES = 1
SPECIAL_COUNT_GAUSS_MIXES = {'SIL': 1}

class AcoModel:
    """  """

    def dist(self, ftr_input):
        # return np.sqrt((ftr_input - self.trained_phoneme)**2)
        # vec1 = np.multiply(ftr_input,            [1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        # vec2 = np.multiply(self.trained_phoneme, [1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

        # return min(np.linalg.norm(ftr_input - _trained_ph) for _trained_ph in self.trained_phoneme)
        #return -4.0 * self.trained_phoneme.loglikelyhood(ftr_input)
        return -4.0 * self.trained_phoneme.score(np.array(ftr_input).reshape(1, -1))

        # vec1 = np.multiply(ftr_input           , coeff_vector)
        #
        # trained_ph_with_coeff = []
        # for _trained_ph in self.trained_phoneme:
        #     trained_ph_with_coeff.append(np.multiply(_trained_ph
        #                                              , coeff_vector))
        #
        # return min(np.linalg.norm(vec1 - _trained_ph) for _trained_ph in trained_ph_with_coeff)

        # return np.linalg.norm(ftr_input - self.trained_phoneme)
        # return np.linalg.norm(vec2 - vec1)

    def __init__(self, sort_dir, fn_sort_file):

        self.name = fn_sort_file

        self.trained_phoneme \
            = self.train_phoneme(AcoModel.read_phoneme(sort_dir + '/' + fn_sort_file), fn_sort_file)

    @staticmethod
    def calc_euclid_dist(ftr1, ftr2):
        return np.linalg.norm(ftr1 - ftr2)

    def train_phoneme(self, phoneme_vectors, filename):
        """ Train a phoneme """

        print("Start to train a phoneme : ", filename)

        ''' raise an exception if has not any feature vector in a file '''
        if len(phoneme_vectors) == 0:
            raise RuntimeError('Phoneme vectors has zero len')

        '''' Get count of clusters '''
        count_clusters = SPECIAL_COUNT_GAUSS_MIXES.get(self.name, None) or NUM_GAUS_MIXES
        # count_clusters = special_count_clusters.get(self.name, None) or default_count_clusters
        # count_clusters = NUM_GAUS_MIXES

        estimator = GaussianMixture(n_components=count_clusters, covariance_type='full', max_iter=80, random_state=0)

        ''' Clustering '''
        print("Clustering")
        res, distortion = kmeans(phoneme_vectors, count_clusters)

        # Since we have class labels for the training data, we can
        # initialize the GMM parameters in a supervised manner.
        estimator.means_init = np.array(res)

        # Train the other parameters using the EM algorithm.
        estimator.fit(phoneme_vectors)

        return estimator

        # # for _idx in range(len(phoneme_vectors)):
        # #     phoneme_vectors[_idx] = np.multiply(phoneme_vectors[_idx], coeff_vector)
        #
        #
        #
        # by_clusters = [[] for _ in range(NUM_GAUS_MIXES)]
        #
        # print("split by clusters")
        # for _ftr in phoneme_vectors:
        #     cluster_id = np.argmin([np.linalg.norm(_cl_mean - _ftr) for _cl_mean in res])
        #
        #     by_clusters[cluster_id].append(_ftr)
        #
        # ph_mean = np.zeros([NUM_GAUS_MIXES, len(phoneme_vectors[0])])
        # ph_vars = np.zeros([NUM_GAUS_MIXES, len(phoneme_vectors[0])])
        # coeff = np.zeros(NUM_GAUS_MIXES)
        #
        # print("start to calc gauss")
        # for cluster_id in range(NUM_GAUS_MIXES):
        #
        #     ph_mean[cluster_id] = np.mean(by_clusters[cluster_id], axis=0)
        #     ph_vars[cluster_id] = np.var(by_clusters[cluster_id], axis=0)
        #
        #     coeff[cluster_id] = float(len(by_clusters[cluster_id])) / len(phoneme_vectors)
        #
        #
        # # res, distortion = kmeans(phoneme_vectors, count_clusters)
        #
        # print("Return gauss")
        # return gmm.DiagGauss(coeff, ph_mean, ph_vars)

    @staticmethod
    def read_phoneme(file_name):

        phoneme = []

        for _ftr_vector in open(file_name).readlines():
            phoneme.append(_ftr_vector.strip().split())

        return np.array(phoneme, dtype=np.float)


class AcoModelSet:
    """  """

    def find_model(self, model_name):
        return self.name2model[model_name]

    def save(self, fname):
        with open(fname, "wb") as f:
            pickle.dump(self, f)

    def __init__(self, sort_dir, save_to_file):

        self.name2model = {}

        print("Training...")
        for fn_model in os.listdir(sort_dir):
            self.name2model[fn_model] = AcoModel(sort_dir, fn_model)

        print("...saving")
        self.save(save_to_file)
        print("Done")

    def print_all(self):
        for _, _model in self.name2model.items():
            print('model "{}" : {}'.format(_model.name, _model.trained_phoneme))

    @staticmethod
    def load_aco_model_set(fname):

        with open(fname, "rb") as f:
            q = pickle.load(f)

        return q

    def getUgidCount(self):
        return len(self.name2model)

    def getDist(self, ugid, ftr):
        return self.name2model[ugid].dist(ftr)


if __name__ == '__main__':
    AcoModelSet('sort_dir', 'data/ph_models')

    model = AcoModelSet.load_aco_model_set('data/ph_models')
    print(model.find_model('a').name)
    print(model.find_model('a').trained_phoneme)

    model.print_all()


# TEST
#
# AcoModelSet('data/sort_dir', 'data/ph_models')
#
# model = AcoModelSet.load_aco_model_set('data/ph_models')
# print(model.find_model('a').name)
# print(model.find_model('a').trained_phoneme)
#
# model.print_all()


# TEST VoxForge
#
# AcoModelSet('VoxForge/sort_dir', 'data/ph_models')
#
# model = AcoModelSet.load_aco_model_set('data/ph_models')
# print(model.find_model('a').name)
# print(model.find_model('a').trained_phoneme)
#
# model.print_all()