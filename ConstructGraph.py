
from collections import namedtuple
from collections import deque

import sys
from AcoModels import AcoModel
from AcoModels import AcoModelSet


class Graph:

    Node = namedtuple('Node', ['model',
                               'word',
                               'is_final',
                               'links_to_next_state'])

    Link = namedtuple('Link', ['state'])

class ConstructGraph:

    @staticmethod
    def costruct(file_dic_name, aco_model_set):
        """  """

        ''' Create first node '''
        head = Graph.Node(model=None, word='', is_final=False, links_to_next_state=[])

        ''' Read new word from dic '''
        for _word in open(file_dic_name).readlines():
            if len(_word.strip()) > 0:
                ConstructGraph.create_word_nodes(_word, aco_model_set, head)

        return head

    @staticmethod
    def create_word_nodes(word_from_dic, aco_model_set, head):

        word, *phonemes = word_from_dic.strip().split()
        # print('word: ', word)
        # print('phonemes: ', phonemes)

        prev_node = head

        ''' Idx of last elem to set final and word '''
        last_elem_idx = len(phonemes) - 1

        for _idx, _ph in enumerate(phonemes):

            ''' Create a new node '''
            # print('idx : {} , ph_num : {}'.format(_idx, len(phonemes)))
            if _idx != last_elem_idx:
                new_node = Graph.Node(model=aco_model_set.find_model(_ph),
                                      word='', is_final=False, links_to_next_state=[])
            else:
                new_node = Graph.Node(model=aco_model_set.find_model(_ph),
                                      word=word, is_final=True, links_to_next_state=[])

            ''' Add loop back '''
            new_node.links_to_next_state.append(Graph.Link(state=new_node))

            ''' Set pointer to new node '''
            prev_node.links_to_next_state.append(Graph.Link(state=new_node))

            ''' Move to next node '''
            prev_node = new_node

        ''' To create a cycle add link to head '''
        prev_node.links_to_next_state.append(Graph.Link(state=head))


    @staticmethod
    def print_graph(node, head):

        model_name = "None"
        if node.model:
            model_name = node.model.name

        print('-' * 30)
        print('name: {} \nword: {}, is_final: {},\nnext_states {}'
              .format(model_name, node.word, node.is_final,
                      ConstructGraph.get_next_states_name(node, head),
                      # [id(_state) for _state in ConstructGraph.get_next_states_name(node, head)],
                      # node.model.trained_phoneme if node.model else []
                      )
              )

        for _next_node in node.links_to_next_state:
            if id(_next_node.state) != id(head) and id(node) != id(_next_node.state):
                ConstructGraph.print_graph(_next_node.state, head)

    @staticmethod
    def get_next_states_name(node, head):
        return [_next_node.state.model.name for _next_node in node.links_to_next_state if _next_node.state != head]


def dfs(root):

    cl = {id(root)}

    next_node = [root]

    while len(next_node):
        print(len(next_node))

        node = next_node[-1]
        next_node.pop()

        for _link in node.links_to_next_state:
            if id(_link.state) not in cl:
                cl.add(id(_link.state))
                next_node.append(_link.state)



#TEST

#
# sys.setrecursionlimit(1000)
#
# aco_model = AcoModelSet.load_aco_model_set('data/VoxForge/ph_models_MIX_sk_2')
# head_node = ConstructGraph.costruct('data/OnlyTestWordsNoOOV.dic', aco_model)
#
# # dfs(head_node)
#
# print('\n' + '#' * 30 + '\n' + ' Debug '.center(30, '~') + '\n' + '#'*30)
# ConstructGraph.print_graph(head_node, head_node)

